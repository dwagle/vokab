#!/usr/bin/env python

import os
import random 
import cPickle 

from django.conf import settings

class Vokab(object):
    '''A simple game that tries to improve your vocabulary a bit '''
    def __init__(self):
        self.dictionary = {}     # mapping of words and their meanings
        self.words = []          # list of words used
        self.secret_word = ''     
        self.length = 0          # length of the secret_word 
        self.keys = []           # the elements of the prompt presented to the user 
        self.mistakes = 0
        self.chances = 0
        self.message = "start clickin!"
        self.end_of_game = False
        
        self.load_dict()
        
    #insert some random hints for the player
    def insert_hints(self, length):
        randint = random.randint
        
        # 3 hints
        if length >= 7: hint = 3
        else: hint = 1
        for x in xrange(hint):
                a = randint(1, length - 1)
                self.keys[a-1] = self.secret_word[a-1]

    def test_input(self, l):
        if self.chances == 0:
            self.end_of_game = True
            self.message = "Sorry You Lost :("
            self.keys = list(self.secret_word)
            return False

        
        #if the guessed letter matches
        if l in self.secret_word:
            indexes = [i for i, item in enumerate(self.secret_word) if item == l]
            for index in indexes:
                self.keys[index] = l
                self.message = "letter matched; chances left: {}".format(self.chances)
            if '_' not in self.keys:  # if all letters guessed
                self.message = "Well Done!"
                self.end_of_game = True

            return True
       
        #if the guessed letter didn't match
        else:
            self.mistakes += 1
            self.chances -= 1
            self.message = "letter didn't match. chances left: {}".format(self.chances)
        
            return False
    

    # load the pickled word dictionary and unpickle them    
    def load_dict(self):
        try :
            dictfile = open(os.path.join(settings.BASE_DIR,"wordsdict.pkl"), "r")
        except IOError:
            print "It seems that someone was messing with the 'wordsdict.pkl' file"
            quit()
        self.dictionary = cPickle.load(dictfile)
        self.words = self.dictionary.keys()
        dictfile.close()
    
    #randomly choose a word for the challenge
    def prepare_word(self):
        
        self.secret_word = random.choice(self.words)
        #don't count trailing spaces
        self.length = len(self.secret_word.rstrip())
        self.keys = ['_' for x in xrange(self.length)]
        self.insert_hints(self.length)
        self.chances = self.length//2

    def show_status(self):
        return ' '.join(self.keys)+" :: "+self.dictionary[self.secret_word] 
    def get_secret_word(self):
        return self.secret_word
         
def create_new_game():
    game_instance = Vokab()
    game_instance.prepare_word()
    return game_instance
