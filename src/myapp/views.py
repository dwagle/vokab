from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponse
from . import vokab
import json

# Create your views here.

# The game instance
v = vokab.create_new_game()

class GameView(View):

    def get(self, request):
        
        status = v.show_status()
        message = v.message 
        eog = v.end_of_game
        return render(request, "index.html", context={"status":status, 'message': message, 'eog': eog})
        #return HttpResponse("<h2>Hangman<h2><h4>A simple word guessing game to improve your vocabulary</h4>")
    def post(self, request):
        # We have to manipulate the game instance so vokab should be a global variable
        global v  
        
        key = request.POST.get('key', None)
        
        if key == 'reset':
            v = vokab.create_new_game()
        elif key == None:
            print("I shouldn't be here")
        else:
            v.test_input(key.lower())
        #print(request.session.__dict__)
        message = v.message
        eog = v.end_of_game
        stat = v.show_status()
        return HttpResponse(json.dumps({'message': message, 'status': stat, 'eog': eog}), content_type="application/json")
